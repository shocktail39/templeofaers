extends Viewport

onready var currentRoom: Node = load("res://Scenes/Starterareatest.tscn").instance()

func change_room(newRoom: Node, spawnIndex: int):
	#unload current room
	remove_child(currentRoom)
	currentRoom.call_deferred("free")
	#load new room
	currentRoom = newRoom
	add_child(currentRoom)
	currentRoom.spawn_player(spawnIndex)

func _ready():
	add_child(currentRoom)
	currentRoom.spawn_player(0)

# just for testing.  press enter to change to the other temple room
func _process(_delta: float):
	if Input.get_action_strength("ui_accept") >= 0.5:
		print("hi")
		change_room(load("res://Scenes/Troom.tscn").instance(), 0)
