extends Spatial

# both arrays should be the same size.
# rotations are in radians.
export var playerSpawnPositions: PoolVector3Array
export var playerSpawnRotations: PoolVector3Array
onready var player: KinematicBody = get_node("PlayerBody")

func spawn_player(spawnIndex: int):
	if playerSpawnPositions.size() > spawnIndex:
		player.translation = playerSpawnPositions[spawnIndex]
		player.rotation = playerSpawnRotations[spawnIndex]
