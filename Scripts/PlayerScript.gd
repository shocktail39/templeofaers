extends KinematicBody

const gravity: float = -6.0
var gravVelocity: float = 0.0
const jumpVelocity: float = 4.0
const walkingSpeed: float = 3.0
const runningSpeed: float = 9.0
var movementVelocity: Vector3
var mouseSensitivity: float = 1.0
var stickSensitivity: float = 100.0
var previouslyOnFloor: bool
var currentlyOnFloor: bool
onready var cam: Camera = get_node("PlayerCamera")
onready var playerShape: CollisionShape = get_node("PlayerShape")
onready var floorChecker: RayCast = get_node("FloorCheckerCast")

func rotate_player(x: float, y: float, delta: float):
	cam.rotation.x -= deg2rad(y * delta)
	cam.rotation.x = clamp(cam.rotation.x, -.5 * PI, .5 * PI)
	rotation.y -= deg2rad(x * delta)

func move_player(x: float, y: float, running: bool):
	#don't bother if there's no input anyway
	if x or y:
		var inputDirection: Vector2 = Vector2(x, y)
		
		# cut the length of inputDirection to 1 if it's bigger than 1,
		# so that the player doesn't move faster diagonally
		if inputDirection.length_squared() > 1: # length_squared() uses fewer cycles than length()
			inputDirection = inputDirection.normalized()
		# rotate to match the player's rotation.
		inputDirection = inputDirection.rotated(rotation.y);
		# slow down backpedaling
		if inputDirection.y < 0:
			inputDirection.y *= 0.75
		# make the movement vector.
		if running:
			movementVelocity = Vector3(inputDirection.x * runningSpeed, 0, -inputDirection.y * runningSpeed)
		else:
			movementVelocity = Vector3(inputDirection.x * walkingSpeed, 0, -inputDirection.y * walkingSpeed)
		# finally, move the player
		move_and_slide(movementVelocity, Vector3.UP)
	else:
		movementVelocity.x = 0
		movementVelocity.z = 0

func do_jump_and_fall(running: bool, delta: float):
	move_and_slide(Vector3(0, gravVelocity, 0), Vector3.UP)
	previouslyOnFloor = currentlyOnFloor
	floorChecker.force_raycast_update()
	currentlyOnFloor = get_slide_count() > 0 && floorChecker.is_colliding()
	if currentlyOnFloor:
		gravVelocity = -0.00001 # gotta have some downward movement or the previous move_and_collide returns null
	elif previouslyOnFloor:
		# makes it so the player can walk down a slope without triggering an auto jump
		if floorChecker.is_colliding():
			translation.y = floorChecker.get_collision_point().y
			currentlyOnFloor = true
		# auto jump
		elif running:
			gravVelocity = jumpVelocity
	else:
		gravVelocity += gravity * delta

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta: float):
	rotate_player(stickSensitivity * (Input.get_action_strength("camera_right") - Input.get_action_strength("camera_left")), stickSensitivity * (Input.get_action_strength("camera_down") - Input.get_action_strength("camera_up")), delta)

func _physics_process(delta: float):
	move_player(Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"), Input.get_action_strength("ui_up") - Input.get_action_strength("ui_down"), Input.is_action_pressed("run"))
	do_jump_and_fall(Input.is_action_pressed("run"), delta)


# godot doesn't seem to have a way to detect mouse movements in the Input api,
# so unless i'm missing something, i have to do this.
# i really hope i'm missing something, because this is stupid.
func _input(event):
	if event is InputEventMouseMotion:
		var amountMoved: Vector2 = event.relative * mouseSensitivity
		rotate_player(amountMoved.x, amountMoved.y, 1)
