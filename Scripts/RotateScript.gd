extends Spatial

export var rotationalVelocityX: float
export var rotationalVelocityY: float
export var rotationalVelocityZ: float

func _process(delta):
	rotation.x += rotationalVelocityX * delta
	rotation.y += rotationalVelocityY * delta
	rotation.z += rotationalVelocityZ * delta
